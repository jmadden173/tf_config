# My TF2 Config Files

#### Currently Installed
* Toonhud (https://toonhud.com/user/toofta/theme/5EU8F8QP/)
* quake hitsound
* custom.cfg 
* mastercomfigs config (https://github.com/mastercoms/mastercomfig)
    * medium low preset
    * mouse tweaks
    * no extra models
    * no soundscapes
    * no tutorial
